import React, { Suspense } from 'react'
import TodosList from './(users)/todos/TodosList'


function Home() {
  return (
    <div>
      {/* We emulate 2 different API calls one from 
      a shopping trolley and one from a todo list */}

      <Suspense fallback={<p className="text-red-500">Loading the todos...</p>}>
      <h1>Loading todos</h1>
      <div className="flex space-x-2">
        {/* @ts-ignore */}
        <TodosList />
      </div>
      </Suspense>

      <Suspense fallback={<p className="text-yellow-500">Loading the shopping trolley...</p>}>
      <h1>Loading shopping trolley</h1>
      <div className="flex space-x-2">
        {/* @ts-ignore */}
        <TodosList />
      </div>
      </Suspense>

    </div>
  )
}

export default Home