import React from 'react'
import { Todo }  from '../../../typings';
import Link from 'next/link'

const fetchTodos =async () => {

  // Set a timeout to run between 1 and 5 seconds to emulate a call to a database
  const timeout = Math.floor(Math.random() * 5 + 1) *1000
  await new Promise((resolve) => setTimeout(resolve, timeout));

    const res = await fetch("https://jsonplaceholder.typicode.com/todos/")
    const todos: Todo[] = await res.json();
    return todos;
}

async function TodosList() {

    const todos = await fetchTodos() 

  return (
    <>
    {todos.map((todo) => (
        <p key ={todo.id}>
            <Link href = {`/todos/${todo.id}`}>Todo: {todo.id}</Link>
        </p>
    ))}
    </>
  )
}

export default TodosList