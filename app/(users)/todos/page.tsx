import React from 'react'
import TodosList from './TodosList'

function Todos(): JSX.Element {
  return (
    <div>
      <h1>This is where the todos will be listed ...</h1>
    </div>
  )
}

export default Todos;