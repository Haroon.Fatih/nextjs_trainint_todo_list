import React from 'react'
import {Todo} from "../../../../typings";
import {notFound} from 'next/navigation'

type PageProps = {
    params: {
        todoId: string;
    };
}

const fetchTodo =async ( todoId: string) => {
    const res = await fetch (
        `https://jsonplaceholder.typicode.com/todos/${todoId}`,
        //isr up to 60 seconds revalidating the cashe
        // Interestingly this means that the user will 
        // only have information incorrect for up to 60 seconds
        { next: {revalidate: 60}}   
    );

    const todo: Todo = await res.json();
    return todo;
};

async function TodoPage({params: {todoId}}: PageProps) {
    const todo = await fetchTodo(todoId);

    // This if statement below allows us to make sure that if the 
    // todo id is not found then we will have the not found page rendered
    if (!todo.id) return notFound() 

  return (
    <div className="p-10 bg-yellow-200 border-2 m-2 shadow-lg">
        <p>
            #{todo.id}: {todo.title}
        </p>
        <p>Completed: {todo.completed ? "yes": "no"}</p>

        <p className="border-t border-black mt-5 text-right">
            By User: {todo.userId}
        </p>
    </div>
  )
}

export default TodoPage

export async function generateStaticParams() {
    const res = await fetch("https://jsonplaceholder.typicode.com/todos/")
    const todos: Todo[] = await res.json();

    // Data trimmed to avoid the rate limit on the API
    const trimmedTodos = todos.splice(0,10);
    
    // need to return an object like so: [{todoId: '1'}, {todoId: '2'}, ... {todoId: '10'}]
    // note that the id numt be in a string format '1', '2' etc

    return todos.map(todo => ({
        todoId: todo.id.toString(),
    }))
}