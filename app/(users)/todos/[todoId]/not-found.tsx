import React from 'react'

function NotFound() {
  return (
    <div>We can not find the todo you are looking for</div>
  )
}

export default NotFound